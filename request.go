package voki

type Requester[R Responder] interface {
	Completer

	// Request should only run if Complete() is true
	Request(*Voki) (R, error)
}
