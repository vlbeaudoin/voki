<img src="voki.svg" alt="voki" width="240"/>

<a href="https://pkg.go.dev/codeberg.org/vlbeaudoin/voki/v3"><img src="https://pkg.go.dev/badge/codeberg.org/vlbeaudoin/voki.svg" alt="Go Reference" width="160"/></a> <a href="https://codeberg.org/vlbeaudoin/voki"><img src="codeberg-badge.png" alt="voki on Codeberg" width="120"/></a>

## vlbeaudoin/voki

Strongly-typed JSON HTTP API request-response framework for go using only the standard library

### Features

- JSON HTTP API caller `(*voki.Voki).Call` with support for bearer tokens in header;

- JSON HTTP API response unmarshaller `(*voki.Voki).Unmarshal` which does JSON decoding on top of `Call`;

- Dedicated JSON API response types with `voki.Responder`;

- Dedicated JSON API request types linked to a response type with `voki.Requester[R voki.Responder]`;

- Reusable and embeddable structs for a basic response type with `voki.Response`;

### Breaking changes in `v3.x.x`

For more details see the [pull-request for `version-3`](https://codeberg.org/vlbeaudoin/voki/pulls/11)

### Breaking changes in `v2.x.x`

For more details, see the [pull-request for `version-2`](https://codeberg.org/vlbeaudoin/voki/pulls/5)

#### Packages `request` and `response` moved to base `voki` package

From this change on, all types, methods and functions declared are accessible in the `voki.*` package.

This was the main reason for moving to version 2 with breaking changes, is that now the interfaces are more ingrained in the `voki` name. Also, this prevents the silly overlap of `request` and `response` concepts in http with voki's concept of processed `Request` and `Response` types.

It seems clearer to me that a `voki.Request` is not necessarily equivalent to a `http.Request`, which was a point of confusion in the module.

#### Interface `voki.Complete` renamed to `voki.Completer`

More uniform with usual naming scheme of single-method interfaces.

### Resources

- Issues: https://codeberg.org/vlbeaudoin/voki/issues

- Repository: https://codeberg.org/vlbeaudoin/voki

- Go module documentation: https://pkg.go.dev/codeberg.org/vlbeaudoin/voki

## Attributions

The voki logo was made using [caro-asercion/rotary-phone](https://game-icons.net/1x1/caro-asercion/rotary-phone.html) which is licensed under [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/).

The voki Codeberg badge was made using [GetItOnCodeberg](https://get-it-on.codeberg.org/)
