/*
Package voki provides a strongly-typed JSON HTTP API request-response framework

It is lightweight and built directly on top of the Go standard library
*/
package voki

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

/*
Completer is a single-method interface to allow structs to declare themselves as `Complete` by returning an boolean.

It is used by `Voki.UnmarshalIfComplete` by checking some or all of their fields and returning `True` if they consider themselves ready to be sent to an API server, or `False` if not.
*/
type Completer interface {
	Complete() bool
}

/*
Voki is the base of the project, and contains a reuseable *http.Client for API requests, along with the connection information for the API server to be queried.

It also implements methods to make calls against that API server with (*Voki).Call, as well as unmarshalling the result into a struct with (*Voki).Unmarshal.

It is recommended to use New() to instanciate a pointer to a new Voki client.
*/
type Voki struct {
	Client   *http.Client
	Host     string
	Key      string
	Port     int
	Protocol string
}

// Complete checks if the different fields of a `Voki` instance are ready for http requests
func (v *Voki) Complete() bool {
	return v.Client != nil && v.Host != "" && v.Key != "" && v.Port != 0 && v.Protocol != ""
}

func New(client *http.Client, host, key string, port int, protocol string) *Voki {
	return &Voki{
		Client:   client,
		Host:     host,
		Key:      key,
		Port:     port,
		Protocol: protocol,
	}
}

/*
(*Voki).Call returns a *http.Response and optionally an error, after requesting
an API resource.

This is used by (*Voki).Unmarshal to make the actual request, and that method
should generally be used over (*Voki).Call.

However, should you need access to the underlying *http.Response, you may
use this method yourself instead.
*/
func (v *Voki) Call(method, route string, requestBody io.Reader, useKey bool) (*http.Response, error) {
	var response *http.Response

	endpoint := fmt.Sprintf("%s://%s:%d%s",
		v.Protocol, v.Host, v.Port, route,
	)

	// Create request
	request, err := http.NewRequest(method, endpoint, requestBody)
	if err != nil {
		return response, err
	}

	if useKey {
		if v.Key == "" {
			return response, fmt.Errorf("Call to API required a key but none was provided")
		}

		request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", v.Key))
	}

	if requestBody != nil {
		request.Header.Add("Content-Type", "application/json")
	}

	// Fetch Request
	response, err = v.Client.Do(request)
	if err != nil {
		return response, err
	}

	return response, nil
}

// CallAndParse is a wrapper around (*voki).Call that only returns a statusCode and body instead of a *http.Response.
func (v *Voki) CallAndParse(method, route string, requestBody io.Reader, useKey bool) (statusCode int, body []byte, err error) {
	var resp *http.Response
	resp, err = v.Call(method, route, requestBody, useKey)
	if err != nil {
		return
	}

	statusCode = resp.StatusCode

	body, err = io.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		err = fmt.Errorf("read body: %s", err)
		return
	}

	return
}

// CloseIdleConnections calls the eponymous method on (*Voki).Client
func (v *Voki) CloseIdleConnections() {
	v.Client.CloseIdleConnections()
}

/*
Unmarshal makes a (*Voki).Call() returning only an error. The Call data is put into the
`destination` parameter.

Example:

	// Instantiate a new *http.Client
	client := http.DefaultClient
	defer client.CloseIdleConnections()

	// Instantiate a new *voki.Voki.
	v := voki.New(client, ...) // Replace ... with your API server details

	// MyStruct must match some or all of the fields returned by the API
	// endpoint
	type MyStruct struct {
	    MyInt    int
	    MyString string
	}

	var destinationStruct MyStruct

	// Replace "/v1/myresource" by the API endpoint you are trying to reach
	// relative to the connection informations defined in voki.New(...)
	if err := v.Unmarshal(http.MethodGet, "/v1/myresource", nil, false, &destinationStruct); err != nil {
	  log.Fatal(err)
	}

	fmt.Println(MyStruct) // Output: <content of destinationStruct>
*/
func (v *Voki) Unmarshal(method, route string, requestBody io.Reader, useKey bool, destination any) error {
	callResponse, err := v.Call(method, route, requestBody, useKey)
	if err != nil {
		return err
	}
	defer callResponse.Body.Close()

	body, err := io.ReadAll(callResponse.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, destination)
	if err != nil {
		return err
	}

	return nil
}

func (v *Voki) UnmarshalIfComplete(method, route string, requestBody io.Reader, useKey bool, destination any) error {
	if !v.Complete() {
		return fmt.Errorf("Incomplete *voki.Voki")
	}

	return v.Unmarshal(method, route, requestBody, useKey, destination)
}
