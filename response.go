package voki

import "net/http"

type Responder interface {
	StatusCode() int

	// Body() returns the []byte parsed from the response body.
	//TODO figure out coupling between this and voki response parsing
	//Body() []byte
}

type ResponseNoContent struct{}

func (r ResponseNoContent) StatusCode() int {
	return http.StatusNoContent
}

type MessageResponse struct {
	Message string `csv:"message" json:"message" yaml:"message"`
}

type ResponseOK struct {
	MessageResponse
}

func (r ResponseOK) StatusCode() int {
	return http.StatusOK
}

type ResponseCreated struct {
	MessageResponse
}

func (r ResponseCreated) StatusCode() int {
	return http.StatusCreated
}

type ResponseBadRequest struct {
	MessageResponse
}

func (r ResponseBadRequest) StatusCode() int {
	return http.StatusBadRequest
}

type ResponseNotFound struct {
	MessageResponse
}

func (r ResponseNotFound) StatusCode() int {
	return http.StatusNotFound
}

type ResponseUnauthorized struct {
	MessageResponse
}

func (r ResponseUnauthorized) StatusCode() int {
	return http.StatusUnauthorized
}

type ResponseInternalServerError struct {
	MessageResponse
}

func (r ResponseInternalServerError) StatusCode() int {
	return http.StatusInternalServerError
}
